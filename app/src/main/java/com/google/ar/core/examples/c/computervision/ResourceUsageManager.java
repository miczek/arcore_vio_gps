package com.google.ar.core.examples.c.computervision;

import android.app.Activity;
import android.os.Handler;
import android.os.HandlerThread;
import android.util.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class ResourceUsageManager {
    private HandlerThread mBackgroundThread;
    private Handler mBackgroundHandler;
    private String mlogFile;
    private int mPid;
    private boolean mBackgroundThreadActive = false;

    private final int CPU_USAGE_INDEX = 8;
    private final int MEMORY_USAGE_INDEX = 9;

    public ResourceUsageManager(String logFile) {
        this.mlogFile = logFile;
    }
    public void startBackgroundThread() {
        mBackgroundThread = new HandlerThread("LogUsageBackground");
        mBackgroundThread.start();
        mBackgroundHandler = new Handler(mBackgroundThread.getLooper());
        mPid = android.os.Process.myPid();
        Log.i("CPU_USAGE_THREAD", "My PID: " + mPid);
        mBackgroundThreadActive = true;

        Runnable loggingActivity = new Runnable() {
            @Override
            public void run() {
                try {
                    logCpuUsage();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                if (mBackgroundThreadActive) {
                    mBackgroundHandler.postDelayed(this, 200);
                }
            }
        };
        mBackgroundHandler.post(loggingActivity);
    }

    public void stopBackgroundThread() {
        try {
            mBackgroundThreadActive = false;
            mBackgroundThread.quitSafely();
            mBackgroundThread.join();
            mBackgroundThread = null;
            mBackgroundHandler = null;
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
    private void logCpuUsage() throws IOException {
        Runtime rt = Runtime.getRuntime();
        String[] commandArray = {"top", "-bn1", "-p", String.valueOf(mPid)};
        Process proc = rt.exec(commandArray);

        BufferedReader stdInput = new BufferedReader(new
                InputStreamReader(proc.getInputStream()));

        Log.i("CPU_USAGE_THREAD", "Here is the standard output of the command:\n");
        String s, lastLine = "";
        while ((s = stdInput.readLine()) != null) {
            Log.i("CPU_USAGE_THREAD", s);
            lastLine = s;
        }
        lastLine = lastLine.trim();
        String lastLineAsList[] = lastLine.split("\\s+");
        String timestamp = String.valueOf(System.currentTimeMillis());
        String log_line = timestamp + "," + lastLineAsList[CPU_USAGE_INDEX] + "," + lastLineAsList[MEMORY_USAGE_INDEX];
        ComputerVisionActivity.appendLog(mlogFile, log_line);
    }
}
